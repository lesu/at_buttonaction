package personal.buttonaction;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
    TextView textViewOut;
    Button buttonIn;
    EditText editTextIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }


    private void initComponents() {
        this.textViewOut = (TextView) this.findViewById(R.id.text_view_out);
        this.buttonIn = (Button) this.findViewById(R.id.button_in);
        this.editTextIn = (EditText) this.findViewById(R.id.edit_text_in);

        this.buttonIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = MainActivity.this.editTextIn.getText().toString();
                MainActivity.this.textViewOut.setText(text);
                MainActivity.this.editTextIn.setText("");
            }
        });
    }


}
